const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();
const courseRoutes = require("./routes/courses");
const userRoutes = require("./routes/users.js");
const app = express();
const env = process.env;
const mdbCS = env.CONNECTION_STRING;

app.use(express.json());
app.use('/courses', courseRoutes);
app.use('/users', userRoutes);

app.listen(env.PORT, () => {
	console.log(`Server is running at port ${env.PORT}`);
});
mongoose.connect(mdbCS);
mongoose.connection.once('open', () => {
	console.log("Server is now connected to the database");
});


app.get('/', (req, res) => {
	res.send("Welcome to my Enrico Miguel H. Silvano's App.");
});		