const Course = require('../models/Course');

module.exports.createCourse = (info) => {

	let newCourse = new Course({
		name: info.name,
		description: info.description,
		price: info.price,
		enrollee: info.enrollee
	});	
	return newCourse.save(info).then((success, error) => {
		if (error) {
			return "Failed to save document";
		}
		else {
			return success;
		}
	});

};

module.exports.getAllCourse = () => {
	return Course.find({}).then(result => {
		return result;
	});
};

module.exports.getCourse = (id) => {
	return Course.findById(id).then(result => {
		return result;
	});
};


module.exports.getAllActiveCourses = () => {
	return Course.find({isActive:true}).then(result => {
		return result;
	});
};

module.exports.deleteCourse = (id) => {
	return Course.findByIdAndDelete(id).then(result => {
		return result;
	});
};

module.exports.findByIdAndUpdate = (id, details) => {
	return Course.findByIdAndUpdate(id, details).then((result, error) => {
		if (result) {
			return "File updated";
		}
		else {
			return "Failed";
		}
	});
};

module.exports.deactivateCourse = (id) =>{
	let updates = {
		isActive: false
	}

	return Course.findByIdAndUpdate(id, updates).then((updated, error) => {
		if (updated) {
			return `Course ${updated.name} with Course ID ${id} has been deactivated`;
		}
		else {
			return "Failed to update course"
		}
	});
}

module.exports.reactivateCourse = (id) =>{
	let updates = {
		isActive: true
	}

	return Course.findByIdAndUpdate(id, updates).then((updated, error) => {
		if (updated) {
			return `Course ${updated.name} with Course ID ${id} has been activated`;
		}
		else {
			return "Failed to update course"
		}
	});
}