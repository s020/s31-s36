const exp = require("express");
const controller = require("../controllers/courses");

const route = exp.Router();
module.exports = route;





route.get('/all', (req, res) => {
	controller.getAllCourse().then(result => {
		res.send(result);
	});
});

route.get('/:id', (req, res) => {
	let courseId = req.params.id;
	controller.getCourse(courseId).then(result => {
		res.send(result);
	});
});

route.get('/', (req, res) => {
	controller.getAllActiveCourses().then(result => {
		res.send(result);
	});
});

route.post('/create', (req, res) => {

	let data = req.body;
	controller.createCourse(data).then(outcome => {
		res.send(outcome);
	});
});

route.put('/:id', (req,res) => {
	let name = req.body.name;
	let description = req.body.description;
	let price = req.body.price;
	
	if (name != "" && description != "" && price != "") {
		controller.findByIdAndUpdate(req.params.id, req.body).then(result => {
			res.send(result);
		});
	}
	else {
		res.send("Please provide input for all fields");
	}
	
});

route.put('/:id/deactivate', (req, res) => {
	console.log(req.params.id);
	controller.deactivateCourse(req.params.id).then(result => {
		res.send(result);
	});
});

route.put('/:id/reactivate', (req, res) => {
	console.log(req.params.id);
	controller.reactivateCourse(req.params.id).then(result => {
		res.send(result);
	});
});

route.delete('/:id', (req, res) => {
	controller.deleteCourse(req.params.id).then(result => {
		res.send(result);
	});
});

