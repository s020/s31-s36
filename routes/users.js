const exp = require('express');
const controller = require('../controllers/users.js');
const route = exp.Router();
module.exports = route;


route.get("/all", (req, res) => {
	controller.getAll().then((result,error) => {
		res.send(result);
	});
});

route.post("/register", (req,res) => {
	let user = req.body;
	console.log(user.firstName);
	if (user.firstName == "" && user.lastName == "" && user.email == "" && user.gender == "" && user.password == "" && user.mobileNo == "") {
		res.send("Please fill up all input fields");
	}
	else{
		console.table(user);
		controller.createUser(user).then(result => {
			res.send(result);
		});
		
	}
});

