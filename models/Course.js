const mongoose = require("mongoose");
const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Course name is required"]
	},
	description: {
		type: String,
		required: [true, "Course description is required"]
	},
	price: {
		type: Number,
		required: [true, "Course price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	enrollee: [
		{
			userID: {
				type: String,
				required: [true, "User's ID is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});

const COURSE = mongoose.model("Course", courseSchema);
module.exports = COURSE;